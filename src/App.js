import React, { PureComponent } from 'react';
import './App.css';
import Body from './components/Body/Body';
import Header from './components/Header/Header';
import Item from './components/Item/Item';
import Modal from './components/Modal/Modal';

class App extends PureComponent {
  state = {
    items: [],
    isLoaded: false,
    setActiveModal: false, 
    itemActions: true,
    modalConfig: {},
  }

  getResponse = () => {
    return fetch('http://localhost:3000/db.json')
      .then(response => response.json())
      .then(data => data.items)
      .catch(err => console.log(err.message));
  }

  async componentDidMount() {
    const data = await this.getResponse();
    this.setState({ isLoaded: true, items: [...data] })
  }

  async addToCart(e) {
    e.preventDefault();
    const articleNumber = Number(e.target.classList[1]);
    const data = await this.getResponse();
    const filterDate = await data.filter(e => e.artNum === articleNumber);
    localStorage.setItem('cart#' + articleNumber, JSON.stringify(filterDate));
    const buildCard = await filterDate.map(e => <Item key={e.id} title={e.title} author={e.author} price={e.price} imgSource={e.imgSrc} artNum={e.artNum} color={e.color} />);
    const cartMessageModal = {
      id: "cartMessageModal",
      headerContent: ['Added to Cart'],
      bodyContent: [buildCard],
      footerContent: [<button className="button" onClick={(e) => this.closeModal(e)}>OK</button>],
      closeButton: true,
    }
    this.setState({setActiveModal : true, modalConfig: cartMessageModal});
  }

  async addToWishList(e) {
    e.preventDefault();
    const articleNumber = Number(e.target.classList[1]);
    if(!e.target.previousElementSibling.classList.contains('gold-color')) {
      e.target.previousElementSibling.classList.add('gold-color');
    }
    const data = await this.getResponse();
    const filterDate = await data.filter(e => e.artNum === articleNumber);
    localStorage.setItem('wish#' + articleNumber, JSON.stringify(filterDate));
    const buildCard = await filterDate.map(e => <Item key={e.id} title={e.title} author={e.author} price={e.price} imgSource={e.imgSrc} artNum={e.artNum} color={e.color} />);
    const wishListMessageModal = {
      id: "wishListMessageModal",
      headerContent: ['Added to Wish List'],
      bodyContent: [buildCard],
      footerContent: [<button className="button" onClick={(e) => this.closeModal(e)}>OK</button>],
      closeButton: true,
    }
    this.setState({setActiveModal : true, modalConfig: wishListMessageModal});
  }
  
  closeModal = (e) => {
    e.preventDefault();
    this.setState({setActiveModal : false});
  }

  render() {
    const { items, isLoaded, setActiveModal, modalConfig, itemActions } = this.state; 
    const showItems = items.map(e => <Item
      itemActions={itemActions} 
      addToCart={(e) => this.addToCart(e)}
      addToWishList={(e) => this.addToWishList(e)}
      key={e.id} 
      title={e.title} 
      author={e.author}
      price={e.price} 
      imgSource={e.imgSrc} 
      artNum={e.artNum} 
      color={e.color} />);
    
    return (
      <div className="App">
        <Header />
        <Body isLoaded={isLoaded} showItems={showItems} />
        {setActiveModal ? <Modal key={modalConfig.id} modalConfig={modalConfig} handlerClose={(e) => this.closeModal(e)} /> : null}
      </div>
    )
  }
}

export default App;


