import React, { PureComponent } from 'react';
import './Body.scss';
import PropTypes from 'prop-types';

class Body extends PureComponent {

    render() {
        const { isLoaded, showItems } = this.props;
        return (
            <section className="content">
                <div className="container">
                    <div className="content__inner">
                        {isLoaded ? showItems : null}
                    </div>
                </div>
            </section>
        );
    }
}

Body.propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    showItems: PropTypes.array.isRequired
};

export default Body;