import React, { PureComponent } from 'react';
import './Item.scss';
import PropTypes from 'prop-types';

class Item extends PureComponent {

    render() {
        const { title, author, price, imgSource, artNum, color, addToCart, addToWishList, itemActions } = this.props;
        return (
            <div className="item" style={{color: color}}>
                <img className="item__img" src={imgSource} alt="Item Image" /> 
                {itemActions ? <div className={'item__wish'} >
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill='transparent' stroke='gold'>
                        <path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/>
                    </svg>
                    <div className={"item__wish-overlay" + ' ' + artNum} onClick={addToWishList} />
                </div> : null}
                <div className="item__text-wrapper">
                    <p className="item__title">{title}</p>
                    <p className="item__author">by {author}</p>
                    <p className="item__artNum">art#{artNum}</p>
                    <div className="item__footer">
                        <p className="item__price">${price}</p>
                        {itemActions ? <a className={"item__buy" + ' ' + artNum} href="#" onClick={addToCart}>add to cart</a> : null}
                    </div>
                </div>
            </div>
        );
    }
}

Item.propTypes = {
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    imgSource: PropTypes.string.isRequired,
    artNum: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToWishList: PropTypes.func.isRequired,
    itemActions: PropTypes.bool.isRequired
};

export default Item;