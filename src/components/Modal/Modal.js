import React, { Component } from 'react';
import "./Modal.scss";
import PropTypes from 'prop-types';

class Modal extends Component {
    render() {
        const { handlerClose, modalConfig } = this.props;
        return (
            <div className="modal" onClick={handlerClose}>
                <div className="modal__content" onClick={(e) => e.stopPropagation()}>
                    <div className="modal__header">
                        <p className="modal__header-title">{modalConfig.headerContent}</p>
                        {modalConfig.closeButton ? <a href="#" className="modal__close" onClick={handlerClose}>{String.fromCharCode(0x2716)}</a> : null}
                    </div>
                    <div className="modal__body">{modalConfig.bodyContent}</div>
                    <div className="modal__footer">{modalConfig.footerContent}</div>
                </div>
            </div>
        );
    }

}

Modal.propTypes = {
    handlerClose: PropTypes.func.isRequired,
    modalConfig: PropTypes.object.isRequired
};

export default Modal;